<?php
$options = parse_ini_file('config.ini', true);

main($argv, $options);

function main($argv, $options)
{
    $onlyClear = false;

    if (isset($argv[1]) && ($argv[1] == '-l' || $argv[1] == '-list')){
        showDatabases($options);
        return 0;
    }

    if (!isset($argv[1]) || $argv[1] == '-h' || $argv[1] == '-help'){
        showHelp();
        return 0;
    }

    if (isset($argv[2]) && ($argv[2] == '-c' || $argv[2] == '-clear')){
        $onlyClear = true;
    }else{
        //valid database backup file path
        if (!isset($argv[2])){
            echo "No database backup file given!\n";
            return 1;
        }
        elseif(!file_exists($argv[2])){
            echo "Given file doesn't exists!\n";
            return 1;
        }
    }

    //get database name and check array with aliases
    $databaseToReload = $argv[1];
    if (array_key_exists($databaseToReload, $options['aliases'])){
        $databaseToReload = $options['aliases'][$databaseToReload];
    }

    //get path database backup
    $pathToBackup = $argv[2];

    echo "Choosed database ".$databaseToReload."\n";

    //delete database
    if (isDatabaseExist($databaseToReload, $options)){
        echo "Deleting database...";
        exec("mysql -u ".$options['username']." ".getPasswordIfSet($options['password'])." -e \"DROP DATABASE IF EXISTS $databaseToReload;\"");
        echo "          Done!\n";
    }
    else{
        echo "Database doesn't exists. Creating new with name: \"$databaseToReload\"!\n";
    }

    //create empty database
    echo "Creating new database...";
    exec("mysql -u ".$options['username']." ".getPasswordIfSet($options['password'])." -e \"CREATE DATABASE $databaseToReload
                            DEFAULT CHARACTER SET ".$options['default_character']."
                            DEFAULT COLLATE ".$options['default_collate'].";\"");
    echo "      Done!\n";

    if (!$onlyClear){
        //import database from file
        echo "Importing data from file...";
        exec("mysql -u ".$options['username']." ".getPasswordIfSet($options['password'])." $databaseToReload < $pathToBackup");
        echo "   Done!\n";
    }

    return 0;
}

function isDatabaseExist($database, $options)
{
    $outputLines = null;
    exec("mysql -u ".$options['username']." ".getPasswordIfSet($options['password'])." -e \"show databases;\"", $outputLines);

    foreach ($outputLines as $outputLine){
        if ($outputLine == $database){
            return true;
        }
    }
    return false;
}

function showDatabases($options)
{
    $outputLines = null;
    exec("mysql -u ".$options['username']." ".getPasswordIfSet($options['password'])." -e \"show databases;\"", $outputLines);

    echo "Available databases:\n";
    foreach ($outputLines as $outputLine){
        if (substr($outputLine, 0, strlen($options['prefix'])) == $options['prefix']){
            echo " ".$outputLine."\n";
        }
    }
}

function showHelp()
{
    echo "  Tool for loading databases\n\n";
    echo "  Options:\n";
    echo "    -h | -help | no arguments   || help\n";
    echo "    -l | -list                  || available databases\n";
    echo "    -c | -clear                 || remove everything from database\n";
    echo "                                   give argument instead of database file\n";
}

function getPasswordIfSet($password)
{
    if ($password){
        return "-p".$password;
    }

    return '';
}
