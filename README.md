# **Skrypt do ładowania bazy danych** #

## Przykładowe wywołanie: ##
```
php dbload.php [nazwa_bazy_lub_alias] [ścieżka_do_backupu]
```

## Opcje: ##
Argument  | Działanie
----------|------------
-h -help  |  pomoc 
-l -list  | lista baz  
-c -clear | czyszczenie bazy (usunie bazę i utworzy nową ale bez importowania z pliku) argument należy podać zamiast ścieżki do backupu

## Konfiguracja: ##
W pliku config.ini należy ustawić dane logowania do bazy i ustawienia do porównywania napisów.

Można również ustawić prefix. Będą wyświetlane wtedy tylko bazy z podanym prefiksem. 

W pliku konfiguracyjnym w sekcji [aliases] można też ustawić aliasy dla baz:

```
[aliases]
yourPrefix = yourDatabaseName
anotherOne = yourDatabaseName
```
* Wpisując aliasy używaj pełnej nazwy bazy (wraz z prefiksem)
* Można ustawić wiele prefiksów dla jednej bazy